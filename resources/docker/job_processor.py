import boto3
import os
import json


def receive_message(aws_region, queue_url):
    sqs_client = boto3.client("sqs", region_name=aws_region)
    response = sqs_client.receive_message(
        QueueUrl=queue_url,
        MaxNumberOfMessages=3,
        WaitTimeSeconds=10,
    )

    print(f"Number of messages received: {len(response.get('Messages', []))}")

    for message in response.get("Messages", []):
        message_body = message["Body"]
        print(f"Message body: {json.loads(message_body)}")
        print(f"Receipt Handle: {message['ReceiptHandle']}")


print("my job processor started...")

aws_region = os.environ['aws_region']
queue_url = os.environ['queue_url']
receive_message(aws_region, queue_url)
