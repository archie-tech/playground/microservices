#!/bin/bash

# Exit on first error
set -e

# Install packages
apt-get update
apt-get -y install python3 python3-pip

# Install Python modules
pip3 install boto3