# Microservices Playground

## Create and push Docker image to private ECR

Build

* docker build --rm --tag my_ecr_image .

Test and inspect

* docker run -idt --name=my_ecr_container my_ecr_image
* docker exec -it my_ecr_container /bin/bash
* docker ps --all --format "table {{.Image}}\t{{.Status}}\t{{.Names}}"
* docker image list

Tag

* set $aws_account and $aws_region
* docker tag my_ecr_image:latest $aws_account.dkr.ecr.$aws_region.amazonaws.com/my_ecr_repository:latest

Push

* aws --profile=archie-tech ecr get-login-password --region $aws_region | docker login --username AWS --password-stdin $aws_account.dkr.ecr.$aws_region.amazonaws.com
* docker push $aws_account.dkr.ecr.$aws_region.amazonaws.com/my_ecr_repository:latest
