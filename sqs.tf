resource "aws_sqs_queue" "q" {
  name = "my_queue"
  tags = {
    Name = "${var.Application} ${var.Customer} ${var.Environment} Queue"
  }
}

output "queue_url" {
  value = aws_sqs_queue.q.url
}
