import boto3
import json
import os


def lambda_handler(event, context):
    send_message(payload=event['body'])
    run_task(task_definition='sample-fargate')
    return {
        "statusCode": 200
    }


def send_message(payload):
    print('sending message to queue ' + os.environ['queue_url'])
    sqs_client = boto3.client("sqs", region_name=os.environ['aws_region'])
    response = sqs_client.send_message(
        QueueUrl=os.environ['queue_url'],
        MessageBody=json.dumps(payload)
    )
    print(response)


def run_task(task_definition):
    print('running fargate task ' + task_definition)
    client = boto3.client('ecs', region_name=os.environ['aws_region'])
    response = client.run_task(
        cluster='cluster-one',
        taskDefinition=task_definition,
        launchType='FARGATE',
        count=1,
        networkConfiguration={
            'awsvpcConfiguration': {
                'subnets': os.environ['subnets'].split(","),
                'securityGroups': os.environ['security_groups'].split(","),
                'assignPublicIp': 'ENABLED'
            }
        },
        overrides={
            'containerOverrides': [
                {
                    'name': 'app2',
                    'environment': [
                        {
                            'name': 'queue_url',
                            'value': os.environ['queue_url']
                        },
                        {
                            'name': 'aws_region',
                            'value': os.environ['aws_region']
                        }
                    ]
                }
            ]
        }
    )
