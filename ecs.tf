locals {
  region    = "eu-west-2"
  family    = "sample-fargate"
  log_group = "my/ecs/my_first_task"
}

resource "aws_ecs_cluster" "main" {
  name = "cluster-one"
}

resource "aws_ecr_repository" "r" {
  name = "my_ecr_repository"
  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_ecs_task_definition" "t" {
  family                   = local.family
  cpu                      = 256
  memory                   = 512
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  task_role_arn            = aws_iam_role.r.arn
  execution_role_arn       = aws_iam_role.r.arn
  container_definitions = jsonencode(
    [
      {
        "name" : "app2",
        "image" : "${data.aws_caller_identity.current.account_id}.dkr.ecr.${var.region}.amazonaws.com/my_ecr_repository:latest",
        "essential" : true,
        "entryPoint" : [
          "sh",
          "-c"
        ],
        "command" : [
          "python3 /var/opt/init/job_processor.py"
        ],
        "logConfiguration" : {
          "logDriver" : "awslogs",
          "options" : {
            "awslogs-group" : local.log_group,
            "awslogs-region" : var.region,
            "awslogs-stream-prefix" : local.family
          }
        },
      }
    ]
  )
}

resource "aws_iam_role" "r" {
  name = "my_ecs_role"
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "AssumeRolePolicy",
          "Action" : "sts:AssumeRole",
          "Effect" : "Allow",
          "Principal" : {
            "Service" : "ecs-tasks.amazonaws.com"
          }
        }
      ]
    }
  )
}

resource "aws_iam_policy" "p" {
  name = "my_ecs_policy"
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : [
            "ecr:GetAuthorizationToken",
            "ecr:BatchCheckLayerAvailability",
            "ecr:GetDownloadUrlForLayer",
            "ecr:BatchGetImage",
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:PutLogEvents"
          ],
          "Resource" : "*"
        },
        {
          "Sid" : "ReceiveMessage",
          "Effect" : "Allow",
          "Action" : [
            "sqs:ReceiveMessage"
          ],
          "Resource" : "arn:aws:sqs:eu-west-2:710665691575:my_queue"
        },
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "ecs-task-permissions" {
  role       = aws_iam_role.r.name
  policy_arn = aws_iam_policy.p.arn
}

resource "aws_cloudwatch_log_group" "g" {
  name = local.log_group
}

resource "aws_security_group" "ecs" {
  name   = "ecs"
  vpc_id = aws_vpc.main.id
  # Allow HTTP from anyware
  ingress {
    description = "Allow HTTP from anyware"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # Allow HTTPs from anyware
  ingress {
    description = "Allow HTTPs from anyware"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # allow all outbound
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # tags
  tags = {
    Name        = "ecs cluster"
    Description = "A secutiry group for my ecs cluster server"
  }
}
