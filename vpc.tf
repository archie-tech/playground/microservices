locals {
  vpc_cidr            = "10.1.0.0/16"
  public_subnet_cidr  = "10.1.1.0/24"
  private_subnet_cidr = "10.1.2.0/24"
}

resource "aws_vpc" "main" {
  cidr_block       = local.vpc_cidr
  instance_tenancy = "default"
  tags = {
    Name = "${var.Application} ${var.Customer} ${var.Environment} VPC"
  }
}

# public

resource "aws_subnet" "public" {
  vpc_id     = aws_vpc.main.id
  cidr_block = local.public_subnet_cidr
  tags = {
    Name = "${var.Application} ${var.Customer} ${var.Environment} Public Subnet"
  }
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "${var.Application} ${var.Customer} ${var.Environment} Internet GW"
  }
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }
  tags = {
    Name = "${var.Application} ${var.Customer} ${var.Environment} Public Route Table"
  }
}

resource "aws_route_table_association" "public" {
  subnet_id      = aws_subnet.public.id
  route_table_id = aws_route_table.public.id
}


# private

resource "aws_subnet" "private" {
  vpc_id     = aws_vpc.main.id
  cidr_block = local.private_subnet_cidr
  tags = {
    Name = "${var.Application} ${var.Customer} ${var.Environment} Private Subnet"
  }
}


resource "aws_route_table" "private" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "${var.Application} ${var.Customer} ${var.Environment} Private Route Table"
  }
}

resource "aws_route_table_association" "private" {
  subnet_id      = aws_subnet.private.id
  route_table_id = aws_route_table.private.id
}

/*

resource "aws_eip" "main" {
  vpc = true
  tags = {
    Name = "${var.Application} ${var.Customer} ${var.Environment} EIP"
  }
}

resource "aws_nat_gateway" "main" {
  connectivity_type = "public"
  allocation_id     = aws_eip.main.id
  subnet_id         = aws_subnet.public.id
  depends_on        = [aws_internet_gateway.main]
  tags = {
    Name = "${var.Application} ${var.Customer} ${var.Environment} NAT GW"
  }
}

*/
