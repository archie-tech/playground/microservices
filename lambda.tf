locals {
  service_name = "my_service"
}

resource "aws_lambda_function" "lambda" {
  function_name = local.service_name
  role          = aws_iam_role.lambda.arn
  runtime       = "python3.8"
  handler       = "${local.service_name}.lambda_handler"
  filename      = "${local.service_name}.zip" # zip my_service.zip my_service.py
  environment {
    variables = {
      aws_region      = "eu-west-2",
      subnets         = aws_subnet.public.id,
      security_groups = aws_security_group.ecs.id,
      queue_url       = aws_sqs_queue.q.url
    }
  }
}

resource "aws_iam_role" "lambda" {
  name = local.service_name
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "AssumeRolePolicy",
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : ["lambda.amazonaws.com"]
          },
          "Effect" : "Allow"
        }
      ]
    }
  )
}

resource "aws_iam_policy" "lambda" {
  name = local.service_name
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "ManageLogs",
          "Effect" : "Allow",
          "Action" : [
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:PutLogEvents"
          ],
          "Resource" : "arn:aws:logs:*:*:*"
        },
        {
          "Sid" : "RunEcsTask",
          "Effect" : "Allow",
          "Action" : [
            "ecs:RunTask"
          ],
          "Resource" : "*"
        },
        {
          "Sid" : "SendMessage",
          "Effect" : "Allow",
          "Action" : [
            "sqs:SendMessage"
          ],
          "Resource" : "arn:aws:sqs:eu-west-2:710665691575:my_queue"
        },
        {
          "Sid" : "GetAndPassRole",
          "Effect" : "Allow",
          "Action" : [
            "iam:GetRole",
            "iam:PassRole"
          ],
          "Resource" : "*"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "lambda" {
  role       = aws_iam_role.lambda.name
  policy_arn = aws_iam_policy.lambda.arn
}

