variable "region" {
  default = "eu-west-2"
}

variable "Environment" {
  default = "Dev"
}

variable "Application" {
  default = "Microservices"
}

variable "Customer" {
  default = "Hilel"
}

variable "Creator" {
  default = "Terraform"
}


terraform {
  required_version = ">= 1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.50"
    }
  }
}

provider "aws" {
  region                  = "eu-west-2"
  shared_credentials_file = "%USERPROFILE%/.aws/credentials"
  profile                 = "archie-tech"
  default_tags {
    tags = {
      Environment = var.Environment
      Application = var.Application
      Customer    = var.Customer
      Creator     = var.Creator
    }
  }
}

data "aws_caller_identity" "current" {}
